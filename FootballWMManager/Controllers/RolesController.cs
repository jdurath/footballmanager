﻿using FootballWMManager.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FootballWMManager.Controllers
{
    public class RolesController : Controller
    {
        private ApplicationDbContext context;

        public RolesController()
        {
            context = new ApplicationDbContext();
        }

        // GET: Roles
        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                if (!IsAdminUser())
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

            var Roles = context.Roles.ToList();
            return View(Roles);
        }

        public Boolean IsAdminUser()
        {
            if (User.Identity.IsAuthenticated)
            {
                var user = User.Identity;
                var context = new ApplicationDbContext();
                using (var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context)))
                {
                    var s = UserManager.GetRoles(user.GetUserId());
                    return s[0].ToString() == "Admin" ? true : false;
                }
            }
            return false;
        }
    }
}