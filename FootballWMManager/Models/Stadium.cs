﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootballWMManager.Models
{
    public class Stadium
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
        public string City { get; set; }
    }
}