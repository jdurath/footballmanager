﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootballWMManager.Models
{
    public class Player : Person
    {
        public DateTime BirthDate { get; set; }
        public double Height { get; set; }
        public double Weight { get; set; }
        public string Club { get; set; }
        public DateTime WorkingTime { get; set; }
        public short Goals { get; set; }
        public short OwnGoals { get; set; }
        public virtual ICollection<Game> Games { get; set; }
    }
}