﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FootballWMManager.Models
{
    public enum Role
    {
        Trainer,
        Assistenztrainer,
        Mannschaftsarzt,
        Player
    }

    public abstract class Person
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Synonym { get; set; }
        public Role? Role { get; set; }
        public virtual Team Team { get; set; }
    }
}