﻿using FootballWMManager.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace FootballWMManager.DAL
{
    public class FootballWMContext : DbContext
    {
        public FootballWMContext() : base("FootballWMManagerConnection")
        {
        }

        public DbSet<Person> Persons { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Stadium> Stadiums { get; set; }
        public DbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}