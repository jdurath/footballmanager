﻿using FootballWMManager.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FootballWMManager.Startup))]

namespace FootballWMManager
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            CreateRolesandUsers();
        }

        private void CreateRolesandUsers()
        {
            var context = new ApplicationDbContext();

            using (var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context)))
            {
                using (var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context)))
                {
                    // In Startup iam creating first Admin Role and creating a default Admin User
                    if (!roleManager.RoleExists("Admin"))
                    {
                        // first we create Admin rool
                        var role = new IdentityRole
                        {
                            Name = "Admin"
                        };
                        roleManager.Create(role);

                        //Here we create a Admin super user who will maintain the website

                        var user = new ApplicationUser
                        {
                            UserName = "carlos",
                            Email = "carlos@web.de"
                        };

                        var userPWD = "Simmel_09";

                        var chkUser = UserManager.Create(user, userPWD);

                        //Add default User to Role Admin
                        if (chkUser.Succeeded)
                        {
                            var result1 = UserManager.AddToRole(user.Id, "Admin");
                        }
                    }

                    // creating Creating Manager role
                    if (!roleManager.RoleExists("Manager"))
                    {
                        var role = new IdentityRole
                        {
                            Name = "Manager"
                        };
                        roleManager.Create(role);
                    }

                    // creating Creating Employee role
                    if (!roleManager.RoleExists("Guest"))
                    {
                        var role = new IdentityRole
                        {
                            Name = "Guest"
                        };
                        roleManager.Create(role);
                    }
                }
            }
        }
    }
}